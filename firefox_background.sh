#!/bin/sh

if [ ! $# -eq 1 ] 
then
    echo "Usage ./firefox_background path/to/new/background" 
    exit 666
fi
if [ ! -f "$1" ] 
then
    echo "File $1 not found." 
    exit 666
fi
read -p "Profile directory path (go to about:support, /home/$USER/.mozilla/firefox/...):" Profile_path
# Check for Profile directory
if [ ! -d $Profile_path ] 
then
    echo "Directory $Profile_path not found." 
    exit 666
fi
# Check for chrome directory in Profile
if [ ! -d "$Profile_path/chrome" ] 
then
    echo "Creating chrome and img folders in Profile"
    mkdir "$Profile_path/chrome"
    mkdir "$Profile_path/chrome/img"
fi
cp userContent.css "$Profile_path/chrome"
rm -f "$Profile_path/chrome/img/*"
cp "$1" "$Profile_path/chrome/img"
mv "$Profile_path/chrome/img/$(basename "$1")" "$Profile_path/chrome/img/background.jpg" 

echo "Done. Go to about:config, put toolkit.legacyUserProfileCustomizations.stylesheets to true and reload Firefox"
